#!/bin/sh -x

# TODO All sorts of error handling :|

MOUNT_REPO_BRANCH=v1.x

DEVICE=$1
MAPPING_NAME=$2
SC_KEY_ID=$3

MOUNT_PART="${DEVICE}1"
ENC_PART="${DEVICE}2"
ENC_PART_BASE="$(basename $ENC_PART)"
MAPPER_DEV=/dev/mapper/$MAPPING_NAME

MNT_DIR=$(mktemp -d)
RAMFS_DIR=$(mktemp -d)

MNT_CFG=$MNT_DIR/mount.cfg
KEY_CLEAR=$RAMFS_DIR/key
KEY_ENC=$MNT_DIR/key.enc
PUB_CERT=$RAMFS_DIR/sc-pubcert.pem
PUB_KEY=$RAMFS_DIR/sc-pubkey.pem

# TODO Check that $1 is a legit device and get user verification
parted --script $DEVICE \
    mklabel gpt \
    mkpart primary 1MiB 2MiB \
    mkpart primary 2MiB 100%

# Probably need to do a sync, but if removing this fdisk output or even
# redirecting output to /dev/null, the mkfs will fail. Timing issue probably.
fdisk -l $DEVICE

# -F to force make the filesystem
mkfs -F -t ext4 $MOUNT_PART

# sync so we dont have any issues with umount
mount -t auto -o sync $MOUNT_PART $MNT_DIR

cd $MNT_DIR

git init
git remote add gitlab-https https://gitlab.com/Unkn/ghetto-smartcard-luks-mount.git
git fetch gitlab-https
git checkout $MOUNT_REPO_BRANCH

cd -

mount -t ramfs -o size=1M,sync ramfs $RAMFS_DIR
chmod -R u=rwx,g=,o= $RAMFS_DIR

pkcs15-tool --read-certificate $SC_KEY_ID > $PUB_CERT
openssl x509 -pubkey -in $PUB_CERT -noout > $PUB_KEY
cat $PUB_KEY

# TODO This assumes an RSA 2048 key
dd if=/dev/urandom of=$KEY_CLEAR bs=1 count=245

openssl rsautl -in $KEY_CLEAR -pkcs -encrypt -pubin -inkey $PUB_KEY -out $KEY_ENC
echo "YES" | cryptsetup -y -v --key-file=$KEY_CLEAR luksFormat $ENC_PART
cryptsetup luksOpen --key-file=$KEY_CLEAR $ENC_PART $MAPPING_NAME

shred $KEY_CLEAR
rm $KEY_CLEAR

mkfs -F -t ext4 $MAPPER_DEV
sync
cryptsetup luksClose $MAPPING_NAME

echo "MAPPING_NAME=$MAPPING_NAME" > $MNT_CFG
echo "UUID=$(ls -l /dev/disk/by-partuuid | grep $ENC_PART_BASE | tr ' ' '\n' | tail -n 3 | head -n 1)" >> $MNT_CFG
echo "SC_KEY_ID=$SC_KEY_ID" >> $MNT_CFG
chmod 0444 $MNT_CFG

umount $MNT_DIR
umount $RAMFS_DIR
rm -rf $MNT_DIR
rm -rf $RAMFS_DIR
